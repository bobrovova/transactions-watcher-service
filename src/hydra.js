const hydra         = require('hydra');
const configHydra   = require('./../config/hydra.json');

hydra.init(configHydra);

module.exports = hydra;