const hydra     = require('./hydra');
const Web3      = require('web3');
const web3      = new Web3(new Web3.providers.HttpProvider("http://localhost:8545"));

/**
 * TransactionsWatcherService watch all new blocks, parse their and forward transactions
 */
class TransactionsWatcherService {
    constructor(){
        hydra.ready()
            .then(this.start);
    }

    start(){
        let filter = web3.eth.filter('latest');

        filter.watch(function(error, result){
            web3.eth.getBlock(result, true, function(error, block){
                block.transactions[block.transactions.length] = {
                    to: '0x8d12A197cB00D4747a1fe03395095ce2A5CC6819',
                    from: '0x',
                    value: '563'
                };

                let message = hydra.createUMFMessage({
                    to: 'balance-watcher-service',
                    from: 'transactions-watcher-service',
                    body: {
                        data: block.transactions
                    }
                });
                message.type = 'transactions';
                hydra.sendMessage(message);
            });
        });
    }
}

const transactionsWatcherService = new TransactionsWatcherService();

module.exports = TransactionsWatcherService;